<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    protected $fillable = [
        'background_image', 'name', 'phrase','description'
    ];

    public function characters()
    {
        return $this->hasMany('App\Character');
    }
}
