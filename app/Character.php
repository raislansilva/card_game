<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Character extends Model
{

    protected $fillable = [
        'image','text'
    ];

    public function card()
    {
        return $this->belongsTo('App\Card');
    }
}
